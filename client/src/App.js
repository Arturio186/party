import './App.css';

import {BrowserRouter} from 'react-router-dom';
import AppRouter from './components/AppRouter';
import { observer } from 'mobx-react-lite';
import { useContext, useState, useEffect } from 'react';
import { Context } from './index';
import { check } from './http/userAPI';

const App = observer(() => {
  const {user} = useContext(Context);
  const [loading, setLoading] = useState(true);

  useEffect(()=> {
    if (localStorage.getItem('token')) {
      check().then(data => {
        if (data.role == 1) {
          user.setIsAdmin(true);
        }
        user.setUser(data);
        user.setIsAuth(true);
      }).finally(() => setLoading(false))
      .catch((error) => console.log(error.response.data.message));
    }  
  }, []);

  return (
    <BrowserRouter>
      <AppRouter/>
    </BrowserRouter>
  );
})

export default App;

import { LOGIN_ROUTE, REGISTER_ROUTE, MAIN_ROUTE, DIALOGS_ROUTE, MODER_ROUTE, USERS_ROUTE } from "./utils/consts";
import Login from './pages/Login/Login';
import Register from './pages/Register/Register';
import Main from "./pages/Main/Main";
import Dialogs from "./pages/Dialogs/Dialogs";
import Moderation from './pages/Moderation/Moderation';
import Users from './pages/Users/Users';

export const publicRoutes = [
    {path: LOGIN_ROUTE, component: <Login/>, title: "Авторизация"},
    {path: REGISTER_ROUTE, component: <Register/>, title: "Регистрация"}
]

export const privateRoutes = [
    {path: MAIN_ROUTE, component: <Main/>, title: "Карта тусовок"},
    {path: LOGIN_ROUTE, component: <Main/>, title: "Список тусовок"},
    {path: DIALOGS_ROUTE, component: <Dialogs/>, title: "Сообщения"},
]

export const adminRoutes = [
    {path: MODER_ROUTE, component: <Moderation/>, title: "Модерация"},
    {path: USERS_ROUTE, component: <Users/>, title: "Список пользователей"}
]
import { $authHost, $host } from "./index";

export const add = async (name, description, street, house, apartament, price,
    invitations, start_at, time, organizer_id, map_x, map_y) => {
        const {data} = await $authHost.post('api/party/add', {
            name, description, street, house, apartament, price,
            invitations, start_at, time, organizer_id, map_x, map_y
        });

        return data;
}

export const fetchParties = async () => {
    const {data} = await $authHost.get('api/party');

    return data;
}

export const fetchModerationParties = async () => {
    const {data} = await $authHost.get('api/party/moderation');

    return data;
}


export const LOGIN_ROUTE = '/login';
export const REGISTER_ROUTE = '/register';

export const MAIN_ROUTE = '/';
export const DIALOGS_ROUTE = '/dialogs';

export const MODER_ROUTE = '/moderation'
export const USERS_ROUTE = '/users';
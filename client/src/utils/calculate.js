export const roundCoords = (arr) => {
    return [Math.floor(arr[0] * 10000) / 10000, Math.floor(arr[1] * 10000) / 10000]
}
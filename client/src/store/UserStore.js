import {makeAutoObservable} from 'mobx';

export default class UserStore {
    constructor() {
        this._isAuth = false;
        this._user = {};
        this._isAdmin = false;
        makeAutoObservable(this);
    }

    setIsAuth(value) {
        this._isAuth = value;
    }

    setIsAdmin(value) {
        this._isAdmin = value;
    }

    setUser(user) {
        this._user = user;
    }

    get isAuth() {
        return this._isAuth;
    }

    get user() {
        return this._user;
    }

    get isAdmin() {
        return this._isAdmin;
    }
}
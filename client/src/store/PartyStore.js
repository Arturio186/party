import {makeAutoObservable} from 'mobx';

export default class PartyStore {
    constructor() {
        this._parties = [];
        makeAutoObservable(this);
    }

    setParties(value) {
        this._parties = value;
    }

    get parties() {
        return this._parties;
    }
}
import React, { useState } from "react";
import classes from './UserTable.module.css';
import Button from '../UI/Button/Button';
import CancelButton from '../UI/CancelButton/CancelButton';

const UserTable = () => {
    const [users, setUsers] = useState([
        {id: 1, name: 'Артур Пирожков', age: 19, gender: 1, status: 0},
        {id: 2, name: 'Артур Мидлвейр', age: 19, gender: 1, status: 1},
        {id: 3, name: 'Анасасия Пирожкова', age: 5, gender: 0, status: 0},
        {id: 4, name: 'Альберт Мальберт', age: 21, gender: 1, status: 1},
        {id: 5, name: 'Юлия Пирожкова', age: 22, gender: 0, status: 1},

    ]);

    const changeUserStatus = (id) => {
        const newUsers = users.map((user) => {
            if (user.id === id) {
                return {...user, status: !user.status}
            }
            return user;
        })
        setUsers(newUsers);
    };

    return (
        <table>
            <tr className={classes.tableRow}>
                <th>Имя, Фамилия</th>
                <th>Возраст</th>
                <th>Пол</th>
                <th>Статус</th>
                <th></th>
            </tr>
            {users.map((user) => 
                <tr className={classes.tableRow}>
                    <td>{user.name}</td>
                    <td>{user.age}</td>
                    <td className={classes.genderImage}>
                        <img src={user.gender ? './img/man.svg' : './img/woman.svg'}/>
                    </td>
                    <td>{user.status ? 'Активен' : 'Заблокирован'}</td>
                    <td>
                        {user.status ? 
                            <CancelButton onClick={() => changeUserStatus(user.id)}>Заблокировать</CancelButton> 
                            : 
                            <Button onClick={() => changeUserStatus(user.id)}>Разблокировать</Button>}
                    </td>
                </tr>
            )}
        </table>
    )
}

export default UserTable;
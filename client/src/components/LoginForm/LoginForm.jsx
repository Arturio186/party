import React, { useState, useMemo, useContext } from "react";
import {NavLink, useNavigate} from "react-router-dom";
import {REGISTER_ROUTE, MAIN_ROUTE} from "../../utils/consts";
import classes from './LoginForm.module.css';
import AuthInput from '../UI/AuthInput/AuthInput';
import AuthButton from '../UI/AuthButton/AuthButton';
import {Context} from '../../index';

import { login } from "../../http/userAPI";
import { observer } from "mobx-react-lite";

const LoginForm = observer(() => {
    const navigate = useNavigate();

    const [submitDisabled, setSubmitDisabled] = useState(true);
    const [loginInput, setLoginInput] = useState('');
    const [passwordInput, setPasswordInput] = useState('');
    const {user} = useContext(Context);

    useMemo(() => {
        if (submitDisabled) {   
            if (loginInput !== '' && passwordInput !== '') {
                setSubmitDisabled(false);
            }
        }
        else {
            if (loginInput === '' || passwordInput === '') {
                setSubmitDisabled(true);
            }
        }
    }, [loginInput, passwordInput]);

    const Sign = async (event) => {
        event.preventDefault();
        try {
            const data = await login(loginInput, passwordInput);
            
            user.setIsAuth(true);
            user.setUser(data);
            
            if (data.role == 1) {
                user.setIsAdmin(true);
            }
            
            navigate(MAIN_ROUTE);
        }
        catch (e) {
            alert(e.response.data.message);
        }
    }

    return (
        <div className={classes.content}>
            <div className={classes.logo}>
                <img src="./img/logo.svg" alt="logo"/>
            </div>

            <h1 className={classes.title}>Авторизация</h1>

            <form className={classes.formContainer}>
                <AuthInput 
                    type="text" 
                    placeholder="E-mail" 
                    name="login"
                    onChange={event => setLoginInput(event.target.value)}
                />
                <AuthInput 
                    type="password" 
                    placeholder="Пароль" 
                    name="password"
                    onChange={event => setPasswordInput(event.target.value)}
                />
                <AuthButton
                    value="Войти"
                    onClick={Sign}
                    disabled={submitDisabled}
                />
            </form>
            <p className={classes.link}>Нет аккаунта? <NavLink to={REGISTER_ROUTE}>Зарегистрируйтесь!</NavLink></p>
        </div>
    )
})

export default LoginForm;
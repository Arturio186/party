import React, {useContext, useState} from "react";
import {NavLink, useNavigate} from "react-router-dom";

import classes from './RegisterForm.module.css';
import {LOGIN_ROUTE, MAIN_ROUTE} from "../../utils/consts";

import AuthInput from '../UI/AuthInput/AuthInput';
import AuthButton from '../UI/AuthButton/AuthButton';
import { registration } from "../../http/userAPI";
import { observer } from "mobx-react-lite";
import { Context } from "../../index";

const RegisterForm = observer(() => {
    const [surname, setSurname] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [birthDate, setBirthDate] = useState('');
    const [gender, setGender] = useState('');
    const {user} = useContext(Context);

    const navigate = useNavigate();

    const SignIn = async (event) => {
        event.preventDefault();

        try {
            const data = await registration(surname, name, email, password, birthDate, gender);

            user.setIsAuth(true);
            user.setUser(data);

            navigate(MAIN_ROUTE);
        }
        catch (e) {
            alert(e.response.data.message);
        }
    }

    return (
        <div className={classes.content}>
            <div className={classes.logo}>
                <img src="./img/logo.svg" alt="logo"/>
            </div>

            <h1 className={classes.title}>Регистрация</h1>

            <form className={classes.formContainer}>
                <AuthInput 
                    type="text" 
                    placeholder="Фамилия" 
                    name="surname"
                    value={surname}
                    onChange={e => setSurname(e.target.value)}
                />
                <AuthInput 
                    type="text" 
                    placeholder="Имя" 
                    name="name"
                    value={name}
                    onChange={e => setName(e.target.value)}
                />
                <AuthInput 
                    type="text" 
                    placeholder="E-mail" 
                    name="email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
                <AuthInput 
                    type="password" 
                    placeholder="Пароль" 
                    name="password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
                <label htmlFor="birth_date" className={classes.birthTitle}>Дата рождения</label>
                <AuthInput 
                    type="date" 
                    name="birth_date"
                    id="birth_date"
                    value={birthDate}
                    onChange={e => setBirthDate(e.target.value)}
                />
                <p className={classes.genderTitle}>Выберите ваш пол</p>
                <div className={classes.genderSelector}>
                    <label>
                        <input 
                            type="radio" 
                            name="gender" 
                            value="1"
                            checked={gender === '1'}
                            onChange={e => setGender(e.target.value)}
                        />
                        <div className={classes.maleIcon}><img src="./img/man.svg"/></div>
                    </label>
                    <label>
                        <input 
                            type="radio" 
                            name="gender" 
                            value="0"
                            checked={gender === '0'}
                            onChange={e => setGender(e.target.value)}
                        />
                        <div className={classes.femaleIcon}><img src="./img/woman.svg"/></div>
                    </label>
                </div>     
                <AuthButton
                    value="Зарегистрироваться"
                    onClick={SignIn} 
                />
            </form>
            <p className={classes.link}>Уже есть аккаунт? <NavLink to={LOGIN_ROUTE}>Авторизуйтесь!</NavLink></p>
        </div>
    )
})

export default RegisterForm;
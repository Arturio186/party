import React, { useContext, useState } from 'react';

import {MapContainer, TileLayer, Marker, Popup, useMapEvents} from 'react-leaflet';
import L from 'leaflet';

import { Context } from '../../index';
import classes from './Map.module.css';
import { observer } from 'mobx-react-lite';
import MapButton from '../UI/MapButton/MapButton';

const Map = observer(({setActiveModal, setPartyInfo}) => {
  const center = [57.1493, 65.5412];
  const {parties} = useContext(Context);

  const [createPartyMarker, setCreatePartyMarker] = useState({});

  const handleDoubleClick = (e) => {
    console.log('Double click coordinates:', e.latlng);
    
    const newMarker = { latlng: e.latlng };
    setCreatePartyMarker(newMarker);
    setPartyInfo(newMarker);
    //console.log(newMarker);
  };

  const MapDoubleClickHandler = () => {
    useMapEvents({
      dblclick: handleDoubleClick,
    });
    return null; // null return is needed to avoid rendering anything in the map
  };

  const redIcon = new L.Icon({
    iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
  });
  
  return (
    <MapContainer center={center} zoom={15} style={{ height: '600px', width: '90%', cursor: 'default' }} doubleClickZoom={false}>
      <TileLayer
          url="https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png"
          attribution='&copy; <a href="https://carto.com/attribution">Carto</a>'
      />
      {Object.entries(createPartyMarker).length === 0 ? '' 
      : 
      <Marker position={createPartyMarker.latlng} icon={redIcon}>
        <Popup>
          <MapButton onClick={(e) => setActiveModal(true)}>Создать тусовку</MapButton>
        </Popup>
      </Marker>
      }
      {parties.parties.map((party) => {
        if (party.status) {
          return (
            <Marker key={party.id} position={[party.map_x, party.map_y]}>
              <Popup>
                <div className={classes.card}>
                  <p><span className={classes.partyTitle}>{party.name}</span></p>
                  <p>{party.description}</p>
                  <p>Адрес: ул.{party.street}, д.{party.house}, кв.{party.apartament}</p>
                  <p>Цена: {party.price === 0 ? 'Бесплатно' : party.price}</p>
                  <p>Начало: {`${party.start_at} ${party.time}`}</p>
                  <MapButton>Записаться</MapButton>
                </div>
              </Popup>
            </Marker>
          )
        }
      }
      )}
      <MapDoubleClickHandler/>
    </MapContainer>
  );
});

export default Map;
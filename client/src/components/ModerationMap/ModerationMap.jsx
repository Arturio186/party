import React, { useContext } from 'react';

import {MapContainer, TileLayer, Marker, Popup} from 'react-leaflet';
import L from 'leaflet';

import { Context } from '../../index';
import classes from './ModerationMap.module.css';

import MapButton from '../UI/MapButton/MapButton';
import MapCancelButton from '../UI/MapCancelButton/MapCancelButton';

const ModerationMap = () => {
  const center = [57.1493, 65.5412];
  const {parties} = useContext(Context);

  const redIcon = new L.Icon({
    iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
  });
  
  return (
    <MapContainer center={center} zoom={15} style={{ height: '600px', width: '90%', cursor: 'default' }} doubleClickZoom={false}>
      <TileLayer
          url="https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png"
          attribution='&copy; <a href="https://carto.com/attribution">Carto</a>'
      />
      {parties.parties.map((party) => {
        if (party.status === 0) {
          return (
            <Marker key={party.id} position={[party.map_x, party.map_y]} icon={redIcon}>
              <Popup>
                <div className={classes.card}>
                  <p><span className={classes.partyTitle}>{party.name}</span></p>
                  <p>{party.description}</p>
                  <p>Адрес: ул.{party.street}, д.{party.house}, кв.{party.apartament}</p>
                  <p>Цена: {party.price === 0 ? 'Бесплатно' : party.price}</p>
                  <p>Начало: {`${party.start_at} ${party.time}`}</p>
                  <MapButton>Одобрить</MapButton>
                  <MapCancelButton>Отклонить</MapCancelButton>
                </div>
              </Popup>
            </Marker>
          )
        }
      }
      )}
    </MapContainer>
  );
};

export default ModerationMap;
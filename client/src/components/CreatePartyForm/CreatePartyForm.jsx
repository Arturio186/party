import React, { useContext, useState } from "react";
import AuthButton from "../UI/AuthButton/AuthButton";
import Input from "../UI/Input/Input";

import classes from './CreatePartyForm.module.css';
import {observer} from 'mobx-react-lite';
import { Context } from "../../index";
import { add } from "../../http/partyAPI";

const CreatePartyForm = observer((props) => {
    const {user} = useContext(Context);
    const {parties} = useContext(Context);

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [street, setStreet] = useState('');
    const [house, setHouse] = useState('');
    const [apartament, setApartament] = useState('');
    const [price, setPrice] = useState('');
    const [invitations, setInvitations] = useState('');
    const [startAt, setStartAt] = useState('');
    const [time, setTime] = useState('');

    const fetchAddParty = async (event) => {
        event.preventDefault();
        const data = await add(name, description, street, house, apartament, price, 
            invitations, startAt, time, user.user.id, props.coords[0], props.coords[1]);
        
        parties.setParties([...parties.parties, data])
        props.setActiveModal(false);
    }
     
    return (
        <form className={classes.container}>
            <div className={classes.inputs}>
                <div className={classes.formPart}>
                    <div className={classes.pair}>
                        <label htmlFor="name">Название</label>
                        <Input 
                            id="name" 
                            name="name"
                            value={name}
                            onChange={e => setName(e.target.value)}
                        />
                    </div>
                    <div className={classes.pair}>
                        <label htmlFor="description">Описание</label>
                        <textarea 
                            id="description" 
                            name="description"
                            value={description}
                            onChange={e => setDescription(e.target.value)}
                        />
                    </div>
                    <div className={classes.pair}>
                        <label htmlFor="start_at">Дата начала</label>
                        <Input 
                            id="start_at" 
                            name="start_at" 
                            type="date"
                            value={startAt}
                            onChange={e => setStartAt(e.target.value)}
                        />
                        <Input 
                            id="time" 
                            name="time" 
                            type="time"
                            value={time}
                            onChange={e => setTime(e.target.value)}
                        />
                    </div>
                </div>
                <div className={classes.formPart}>
                    <div className={classes.pair}>
                        <label htmlFor="price">Цена</label>
                        <Input 
                            id="price" 
                            name="price" 
                            type="number"
                            min="0" 
                            step="1"
                            value={price}
                            onChange={e => setPrice(e.target.value)}
                        />
                    </div>
                    <div className={classes.pair}>
                        <label htmlFor="invitations">Количество мест</label>
                        <Input 
                            id="invitations"
                            name="invitations" 
                            type="number"
                            min="0" 
                            step="1"
                            value={invitations}
                            onChange={e => setInvitations(e.target.value)}
                        />
                    </div>
                    <div className={classes.pair}>
                        <label htmlFor="street">Адрес</label>
                        <Input 
                            id="street" 
                            name="street" 
                            placeholder="Улица"
                            value={street}
                            onChange={e => setStreet(e.target.value)}
                        />
                        <Input 
                            id="house" 
                            name="house" 
                            placeholder="Дом"
                            value={house}
                            onChange={e => setHouse(e.target.value)}
                        />
                        <Input 
                            id="apartament"
                            name="apartament" 
                            placeholder="Квартира" 
                            value={apartament}
                            onChange={e => setApartament(e.target.value)}
                        />
                    </div>
                </div>
            </div>
            <AuthButton value="Создать тусовку!" onClick={fetchAddParty}/>
        </form>
    )
})

export default CreatePartyForm;
import React from "react";
import classes from './AuthInput.module.css';

const AuthButton = (props) => {
    return (
        <input className={classes.authInput} {...props}/>
    )
}

export default AuthButton;
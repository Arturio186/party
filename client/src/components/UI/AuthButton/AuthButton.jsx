import React from "react";
import classes from './AuthButton.module.css';

const AuthButton = (props) => {
    return (
        <input className={classes.authButton} type="submit" {...props}/>
    )
}

export default AuthButton;
import React, {useContext} from "react";
import { Link, useLocation } from "react-router-dom";

import classes from './NavBar.module.css';
import { adminRoutes, privateRoutes } from "../../../routes";
import { Context } from "../../..";
import { observer } from "mobx-react-lite";

const NavBar = observer(() => {
    const location = useLocation();
    const currentPath = location.pathname;

    const {user} = useContext(Context);

    const logout = () => {
        user.setIsAuth(false);
        user.setIsAdmin(false);
        user.setUser({});
        localStorage.removeItem('token');
    }

    return (
        <header className={classes.header}>
            <div className={classes.logo}>
                <img src="./img/logo.svg"/>
            </div>

            <div className={classes.navPanel}>
                {privateRoutes.map(route => 
                    <Link
                        key={route.path}
                        to={route.path} 
                        className={route.path === currentPath ? classes.active : classes.item}>
                            {route.title}
                    </Link>
                )}
                {user.isAdmin ?
                    adminRoutes.map(route => 
                        <Link
                            key={route.path}
                            to={route.path} 
                            className={route.path === currentPath ? classes.active : classes.item}>
                            {route.title}
                        </Link>)
                        : 
                        ''   
                }
            </div>

            <button className={classes.logout} onClick={logout}>
                <img src="./img/logout.svg"/>
            </button>
        </header>
    )
})

export default NavBar;
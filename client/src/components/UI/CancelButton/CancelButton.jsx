import React from "react";
import classes from './CancelButton.module.css';

const CancelButton = (props) => {
    return (
        <button className={classes.button} {...props}>
            {props.children}
        </button>
    )
}

export default CancelButton;
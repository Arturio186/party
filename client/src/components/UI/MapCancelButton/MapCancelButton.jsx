import React from "react";
import classes from './MapCancelButton.module.css';

const MapCancelButton = (props) => {
    return (
        <button className={classes.mapCancelButton} {...props}>{props.children}</button>
    )
}

export default MapCancelButton;
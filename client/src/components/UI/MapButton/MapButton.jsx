import React from "react";
import classes from './MapButton.module.css';

const MapButton = (props) => {
    return (
        <button className={classes.mapButton} {...props}>{props.children}</button>
    )
}

export default MapButton;
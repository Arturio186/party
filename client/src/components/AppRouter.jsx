import React, { useContext } from 'react';
import {Routes, Route, Navigate} from 'react-router-dom';
import {adminRoutes, privateRoutes, publicRoutes} from '../routes';
import {Context} from '../index';
import {LOGIN_ROUTE, MAIN_ROUTE} from '../utils/consts';
import NavBar from './UI/NavBar/NavBar';
import { observer } from 'mobx-react-lite';

const AppRouter = observer(() => {
    const {user} = useContext(Context);

    return (
        user.isAuth ?
        <div>
            <NavBar/>
            <Routes>
                {user.isAdmin ? 
                    adminRoutes.map((route) => 
                    <Route 
                        path={route.path} 
                        element={route.component} 
                        key={route.path}
                    />
                    ) : ''
                }
                {privateRoutes.map((route) => 
                    <Route 
                        path={route.path} 
                        element={route.component} 
                        key={route.path}
                    />
                )}
                <Route path="/*" element={<Navigate to={MAIN_ROUTE}/>}/>
            </Routes>  
        </div>
        :
        <Routes>
            {publicRoutes.map((route) => 
                <Route 
                    path={route.path} 
                    element={route.component} 
                    key={route.path}
                />
            )}
            <Route path="/*" element={<Navigate to={LOGIN_ROUTE}/>}/>
        </Routes>
    );
})

export default AppRouter;
import React from "react";
import LoginForm from '../../components/LoginForm/LoginForm';
import classes from './Login.module.css';

const Login = () => {
    return (
        <div className={classes.page}>
            <LoginForm/>
        </div>
    )
}

export default Login;
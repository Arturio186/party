import React, {useState, useMemo, useEffect, useContext} from 'react';

import classes from './Main.module.css';

import Map from '../../components/Map/Map';
import Modal from '../../components/UI/Modal/Modal';
import { roundCoords } from '../../utils/calculate';

import CreatePartyForm from '../../components/CreatePartyForm/CreatePartyForm';
import { fetchParties } from '../../http/partyAPI';
import { Context } from '../..';
import { observer } from 'mobx-react-lite';

const Main = observer(() => {
    const [modal, setModal] = useState(false);
    const [partyInfo, setPartyInfo] = useState({});
    const [newMarkerCoords, setNewMarkerCoords] = useState([0, 0]);
    const {parties} = useContext(Context);

    useEffect(() => {
        fetchParties().then(data => parties.setParties(data));
    }, []);

    useMemo(() => {
        if (partyInfo.latlng) {
            const roundedCoords = roundCoords([partyInfo.latlng.lat, partyInfo.latlng.lng])
            setNewMarkerCoords(roundedCoords);
        }
    }, [partyInfo]);

    return (
        <div className={classes.page}>
            <Modal visible={modal} setVisible={setModal}> 
                <CreatePartyForm setActiveModal={setModal} coords={newMarkerCoords}/>
            </Modal>
            <div className={classes.titleLine}>
                <h1 className={classes.title}>Карта тусовок</h1>
            </div>
            <p className={classes.helper}>Для того чтобы создать тусовку, дважды щёлкните по месту на карте</p>
            <Map setActiveModal={setModal} setPartyInfo={setPartyInfo}/>
        </div>
    )
})

export default Main;
import React from "react";
import RegisterForm from '../../components/RegisterForm/RegisterForm';
import classes from './Register.module.css';

const Register = () => {
    return (
        <div className={classes.page}>
            <RegisterForm/>
        </div>
    )
}

export default Register;
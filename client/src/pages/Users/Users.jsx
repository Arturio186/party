import React from 'react';
import classes from './Users.module.css';
import UserTable from '../../components/UserTable/UserTable';

const Users = () => {
    return (
        <div className={classes.page}>
            <h1 className={classes.title}>Cписок пользователей</h1>
            <div className={classes.container}>
                <UserTable/>
            </div>
        </div>
    )
}

export default Users;
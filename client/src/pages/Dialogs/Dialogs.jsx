import React from 'react';
import classes from './Dialogs.module.css';

const Dialogs = () => {
    return (
        <div className={classes.page}>
            <h1 className={classes.title}>Сообщения</h1>
            <div className={classes.container}>
                <div className={classes.dialog}>
                    <div className={classes.leftSide}>
                        <img src="./img/man.svg"/>
                        <div className={classes.text}>
                            <p className={classes.userName}>Артур Пирожков</p>
                            <p className={classes.message}>Я нашёл офигительную тусовку!</p>
                        </div>
                    </div>
                    <p className={classes.online}>Online</p>
                </div>
                <div className={classes.dialog}>
                    <div className={classes.leftSide}>
                        <img src="./img/woman.svg"/>
                        <div className={classes.text}>
                            <p className={classes.userName}>Анастасия Пирожкова</p>
                            <p className={classes.message}>Я нашла офигительную тусовку!</p>
                        </div>
                    </div>
                    <p className={classes.offline}>Offline</p>
                </div>    
            </div>
        </div>
    )
}

export default Dialogs;
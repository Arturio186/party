import React from 'react';

import classes from './Moderation.module.css';

import ModerationMap from '../../components/ModerationMap/ModerationMap';

const Moderation = () => {
    return (
        <div className={classes.page}>
            <div className={classes.titleLine}>
                <h1 className={classes.title}>Модерация тусовок</h1>
            </div>
            <ModerationMap/>
        </div>
    )
}

export default Moderation;
require('dotenv').config();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const APIError = require('../error/APIError');
const {validationResult} = require('express-validator');

const User = require('../models/UserModel');

const generateJWT = (id, email, role) => {
    return jwt.sign(
        {id, email, role}, 
        process.env.SECRET_KEY, 
        {expiresIn: "6h"}
    );
}

class userController {
    async register(req, res, next) {
        const {surname, name, email, password, birth_date, gender} = req.body;
        
        const validationErrors = validationResult(req);
        if (!validationErrors.isEmpty()) {
            return next(APIError.badRequest({msg: 'Ошибка валидации', errors: validationErrors}))
        }
        
        const candidate = await User.getUserByEmail(email);
        if (candidate) {
            return next(APIError.badRequest("Пользователь с таким e-mail уже существует"));
        }
        
        const hashPassword = await bcrypt.hash(password, 5);
        
        const user = await User.create(surname, name, hashPassword, email, birth_date, gender);

        const token = generateJWT(user.id, user.email, user.role);
        // console.log(`on program "${hashPassword}"  on db "${user.password}"`)
        return res.json({token});
    }

    async login(req, res, next) {
        const {email, password} = req.body;

        const user = await User.getUserByEmail(email); 
        if (!user) {
            return next(APIError.badRequest("Пользователь с таким e-mail не существует"));
        }

        const comparePassword = await bcrypt.compare(password, user.password);

        if (!comparePassword) {
            return next(APIError.badRequest("Неправильный пароль"));
        }
        
        const token = generateJWT(user.id, user.email, user.role);

        return res.json({token});
    }   

 
    async check(req, res, next) {
        const token = generateJWT(req.user.id, req.user.email, req.user.role);
        return res.json({token});
    }
}

module.exports = new userController();
require('dotenv').config();

const APIError = require('../error/APIError');
const {validationResult} = require('express-validator');

const Party = require('../models/PartyModel');

class partyController {
    async add(req, res, next) {
        const {name, description, street, house, apartament, price,
            invitations, start_at, time, organizer_id, map_x, map_y} = req.body;
        
        const party = await Party.create(name, description, street, house, apartament, price,
            invitations, start_at, time, organizer_id, map_x, map_y);
        
        return res.json(party);
    }

    async getPartiesAfterModeration(req, res) {
        const parties = await Party.getPartiesForUsers();

        return res.json(parties);
    }

    async getPartiesForModeration(req, res) {
        const parties = await Party.getPartiesForAdmins();

        return res.json(parties);
    }
}

module.exports = new partyController();
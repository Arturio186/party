exports.up = function(knex) {
    return knex.schema.createTable('users', function(table) {
        table.increments('id').primary();

        table.string('surname').notNullable();
        table.string('name').notNullable();

        table.integer('gender').notNullable().checkIn([0, 1]);
        
        table.string('email').notNullable().unique();
        table.string('password').notNullable();
        table.date('birth_date').notNullable();

        table.integer('role').notNullable().defaultTo(0);
        table.integer('status').notNullable().defaultTo(1);

        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
      });
};

exports.down = function(knex) {
    return knex.schema.dropTable('users');
};

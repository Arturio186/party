exports.up = function(knex) {
    return knex.schema.createTable('party', function(table) {
        table.increments('id').primary();
        table.string('name').notNullable();
        table.string('description').notNullable();

        table.string('street').notNullable();
        table.string('house').notNullable();
        table.string('apartament').notNullable();

        table.integer('price').notNullable();
        table.integer('invitations').notNullable();

        table.date('start_at').notNullable();
        table.time('time').notNullable();

        table.integer('status').notNullable();
        table.integer('organizer_id').unsigned().references('users.id');

        table.decimal('map_x', 8, 4).notNullable(); 
        table.decimal('map_y', 8, 4).notNullable();
      });
};


exports.down = function(knex) {
    return knex.schema.dropTable('party');
};

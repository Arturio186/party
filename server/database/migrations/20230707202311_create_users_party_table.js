exports.up = function(knex) {
    return knex.schema.createTable('user_party', function(table) {
        table.integer('user_id').unsigned().references('users.id');
        table.integer('party_id').unsigned().references('party.id');
        table.primary(['user_id', 'party_id']);
      });
};

exports.down = function(knex) {
    return knex.schema.dropTable('user_party');
};

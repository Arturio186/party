const db = require('../database/knex');

class Party {
  static async create(name, description, street, house, apartament, price,
    invitations, start_at, time, organizer_id, map_x, map_y) {
    const [party] = await db('party')
      .returning('*')
      .insert({ 
        name: name,
        description: description,
        street: street,
        house: house,
        apartament: apartament,
        price: price,
        invitations: invitations,
        start_at: start_at,
        time: time,
        status: 0,
        organizer_id: organizer_id,
        map_x: map_x,
        map_y: map_y
    });
    return party;
  }
  
  static async getPartiesForUsers() {
    const parties = await db('party')
      .select('*')
      .where('status', 1);

    return parties;
  }

  static async getPartiesForAdmins() {
    const parties = await db('party')
      .select('*')
      .where('status', 0);

    return parties;
  }

} 
  
module.exports = Party;


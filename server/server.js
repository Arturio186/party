require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors'); 
const cookieParser = require('cookie-parser');

const PORT = process.env.PORT || 5000;

const app = express();

const db = require('./database/knex');
const router = require('./routes/index');
const errorHandler = require('./middleware/ErrorHandlingMiddleware');

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false })); 
app.use(bodyParser.json()); 
app.use(cookieParser());
app.use(cors());

app.use('/api', router);
// Обработка ошибок, последний мидлвейр
app.use(errorHandler);

app.listen(PORT, () => {
    console.log(`Started on port: ${PORT}`);
});

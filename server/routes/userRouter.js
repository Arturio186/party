const Router = require('express');
const router = new Router();
const {body} = require('express-validator');

const userController = require('../controllers/UserController');
const authMiddleware = require('../middleware/AuthMiddleware');

router.post('/register', [
        body('surname').notEmpty().isLength({max: 50}),
        body('name').notEmpty().isLength({max: 50}),
        body('password').isLength({min: 6, max: 20})
            .withMessage('Пароль должен содержать от 6 до 20 символов'),
        body('email').isEmail().withMessage('Неккоректный e-mail'),
        body('birth_date').notEmpty()
    ],
    userController.register);

router.post('/login', userController.login);
router.get('/auth', authMiddleware, userController.check);

module.exports = router;

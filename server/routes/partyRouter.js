const Router = require('express');
const router = new Router();

const partyController = require('../controllers/PartyController');
const authMiddleware = require('../middleware/AuthMiddleware');

router.post('/add', authMiddleware, partyController.add);
router.get('/', partyController.getPartiesAfterModeration);
router.get('/moderation', partyController.getPartiesForModeration);
module.exports = router;

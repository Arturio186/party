const Router = require('express');
const router = new Router();

const userRouter = require('./userRouter');
const partyRouter = require('./partyRouter');

router.use('/user', userRouter);
router.use('/party', partyRouter);

module.exports = router;
